export default class Education {
  constructor(year, tiltle, description) {
    this._year = year;
    this._tiltle = tiltle;
    this._description = description;
  }
  get year() {
    return this._year;
  }

  set year(value) {
    this._year = value;
  }

  get tiltle() {
    return this._tiltle;
  }

  set tiltle(value) {
    this._tiltle = value;
  }

  get description() {
    return this._description;
  }

  set description(value) {
    this._description = value;
  }
}
