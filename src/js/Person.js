import Education from './Education';

export default class Person {
  constructor(name, age, description, education) {
    this._name = name;
    this._age = age;
    this._description = description;
    this._education = [];
    education.forEach(v => {
      this._education.push(new Education(v.year, v.title, v.description));
    });
  }

  set name(value) {
    this._name = value;
  }

  set age(value) {
    this._age = value;
  }

  set description(value) {
    this._description = value;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get description() {
    return this._description;
  }

  get education() {
    return this._education;
  }
}
