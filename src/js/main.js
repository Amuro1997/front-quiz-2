import $ from 'jQuery';
import Person from './Person';
import Eudcation from './Education';

const url = 'http://localhost:3000/person';

function fetchData(url) {
  return fetch(url).then(res => res.json());
}

fetchData(url).then(res => {
  let { name, age, description, educations } = res;
  let person = new Person(name, age, description, educations);
  console.log(person);
  renderNameAndAge(person);
  renderDescription(person);
  renderEducations(person.education);
});

function renderNameAndAge(person) {
  $('#age').html(person.age);
  $('#name').html(person.name);
}

function renderDescription(person) {
  $('#description').html(person.description);
}

function renderEducations(educations) {
  educations.forEach(education => {
    $('#educationList').append(getString(education) + '<br>');
  });
}

function getString(education) {
  console.log(education);
  return `<li><span id="year">${education.year}</span><div id="block"><h4>${
    education.tiltle
  }</h4><p>${education.description}</p></div></li>`;
}
